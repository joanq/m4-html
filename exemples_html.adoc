= Exemples d'HTML
:doctype: article
:encoding: utf-8
:lang: ca
:numbered:
:ascii-ids:

- link:code/primera_classe.html[Primera classe]

- link:code/html/01_primera_pagina.html[Primera pàgina]
- link:code/html/02_encapcalaments.html[Encapçalaments]
- link:code/html/03_paragrafs.html[Paràgrafs]
- link:code/html/04_enllacos.html[Enllaços]
- link:code/html/05_imatges.html[Imatges]
- link:code/html/06_inline_vs_block.html[Inline versus block]
- link:code/html/07_llistes.html[Llistes]
- link:code/html/08_taules.html[Taules]
- link:code/html/09_entitats.html[Entitats]
- link:code/html/10_text.html[Text]
- link:code/html/11_comentaris.html[Comentaris]
- link:code/html/12_div_i_span.html[Div i Span]
- link:code/html/13_class_i_id.html[Class i Id]
- link:code/html/14_noves_etiquetes.html[Noves etiquetes]
- link:code/html/15_iframes.html[IFrames]
- link:code/html/16_rutes_relatives_i_absolutes.html[Rutes relatives i absolutes]
- link:code/html/17_meta.html[Etiqueta meta]
