= Passos per configurar el GIT
:doctype: article
:encoding: utf-8
:lang: ca
:numbered:
:ascii-ids:

1. Crear usuari a Gitlab.
2. Crear projecte M4 HTML.
3. Crear usuari a Linux.
4. Configurar GIT:
+
[source]
----
git config --global user.email maria@gmail.com
git config --global user.name Maria López
----

5. Crear clau criptogràfica:
+
[source]
----
ssh-keygen -t rsa -C "maria@gmail.com" -b 4096
----

6. Pujar clau criptogràfica al GitLab:
+
- Perfil de l'usuari.
- Claus SSH
- Enganxar clau pública (`.ssh/id_rsa.pub`)

7. Flux de treball bàsic en línia de comandes:
+
1. El primer cop: `git clone git@gitlab.com:joanq/m4-html.git`
2. Per cada grup de canvis:
+
[source]
----
git add .     Marca els fitxers per guardar-los
git commit -m "missatge"    Guarda els fitxers al git local
git push      Puja els canvis al GitLab
----

8. Conflictes
+
--
Un conflicte es produeix quan s'han fet canvis al mateix fitxer a dues branques
diferent i intentem integrar-les.

Per provar-ho podem fer una modificació a un fitxer al Gitlab, i fer una altra
modificació al mateix punt del mateix diferent al repositori local.

Quan fem `git pull` per incorporar els canvis del repositori remot al repositori
local, obtindrem un conflicte.

El git ens marca la posició on s'ha trobat el problema amb unes marques de text
especial, i ens indica les dues versions del text que es volen unit. Per
resoldre el conflicte hem de procedir com amb qualsevol altre canvi que vulguem
fer: editem el fitxer (esborrant les marques de text que ens
ha creat el git, i triant quina de les versions), i fem `git add` i
`git commit`.
--

9. Flux de treball des de l'Atom

Molts editors de text per a desenvolupadors incorporen suport per utilitzar git
des del propi editor.

En l'Atom tenim una pestanya a la dreta que ens permet fer totes les operacions
habituals (`pull`, `push`, `add`, `commit`...) directament.
