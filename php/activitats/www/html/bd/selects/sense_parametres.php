<?php
$server = "localhost";
$username = "webuser";
$password = "super3";
$db = "hotel";

try {
  $conn = new PDO("mysql:host=$server;dbname=$db;charset=utf8", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $statement = $conn->prepare("SELECT FirstName, LastName, Email FROM Customers LIMIT 10");
  $statement->execute();
  $customers = $statement->fetchAll();
} catch(PDOException $e) {
  $error = "No s'ha pogut recuperar la llista de clients:\n{$e->getMessage()}\n";
}
?>
<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title>Consulta sense paràmetres</title>
  </head>
  <body>
    <main role="main" class="container">
      <h1 class="mt-5">Consulta sense paràmetres</h1>
      <?php
      if (isset($error)) {
        echo "<div class='alert alert-warning'>$error</div>";
      }
      if (isset($customers)) {
        echo "<table class='table table-striped'>\n<tr><th>Nom</th><th>Cognom</th><th>Correu electrònic</th></tr>\n";
        foreach ($customers as $customer) {
          echo "<tr><td>{$customer['FirstName']}</td><td>{$customer['LastName']}</td><td>{$customer['Email']}</td></tr>\n";
        }
        echo "</table>\n";
      }
      ?>
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
