<?php
require_once 'Connection.php';

session_start();

try {
  if (!isset($_POST['name'])) {
    throw new Exception("Falten paràmetres.");
  }
  $name = trim($_POST['name']);
  $conn = connect();
  $statement = $conn->prepare("INSERT INTO Facilities(Name) VALUES (:name)");
  $statement->bindParam(':name', $name);
  $statement->execute();
  header('Location: index.php');
  exit();
} catch (Exception $e) {
  $_SESSION['error'] = $e->getMessage();
  header('Location: index.php');
  exit();
}

?>
