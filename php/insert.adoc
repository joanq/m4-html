= Activitat: insercions i eliminacions
:doctype: article
:encoding: utf-8
:lang: ca
:numbered:
:ascii-ids:
:icons: font

Per a cada exercici crea un fitxer dins de la carpeta `html/activitat5` amb nom
`exercici1.php`, `exercici2.php`, etc.

La codificació de la pàgina ha de ser UTF-8, la pàgina ha de validar amb HTML 5
i l'aspecte del codi html que es genera ha de ser fàcilment llegible,
estar ben identat, etc.

[NOTE]
====
Utilitza excepcions per separar les situacions d'error de l'execució exitosa
dels programes.
====

1. Crea una pàgina web que permeti afegir habitacions a l'hotel.
Per això, demana el número d'habitació i mostra un desplegable amb tots els
tipus d'habitació que hi ha. Cal mostrar un error si el número d'habitació ja
existeix.

2. Crea una pàgina web que mostri totes les habitacions que hi ha, amb el seu
número i el tipus d'habitació de què es tracta, i les permeti eliminar. Tingues
en compte que només es podran eliminar habitacions que mai hagin tingut reserves
o estades associades.

3. A partir de l'exercici corresponent de l'activitat de consultes, volem
completar la funcionalitat del nostre formulari de reserves.
+
Modifica l'exercici per tal que, a més de mostrar confirmació de si hi ha o no
habitació disponible, guardi les dades de la reserva.
+
Pel que fa al client, comprovarem si és un client que ja existeix o no a partir
del seu correu electrònic, i si no existeix, el crearem.
